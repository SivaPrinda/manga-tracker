// Footer.js

import React from "react";

const Footer = () => {
  return (
    <footer className="bg-white text-gray-900 p-4">
      <div className="container mx-auto">
        <hr className="my-4 border-t border-gray-300" />
        <p className="text-center">
          &copy; 2023 Mon Site. Tous droits réservés.
        </p>
      </div>
    </footer>
  );
};

export default Footer;

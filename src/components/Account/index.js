import React from "react";

const Account = () => {
  return (
    <div className="min-h-screen bg-gray-100 flex items-center justify-center">
      <div className="bg-white p-8 rounded-lg shadow-md w-full max-w-3xl">
        <div className="flex items-center justify-center">
          <img
            className="h-16 w-16 rounded-full object-cover"
            src="https://placekitten.com/200/200" // Remplacez cela par l'URL de votre image de profil
            alt="Profile"
          />
          <div className="ml-4">
            <h1 className="text-2xl font-bold text-gray-800">John Doe</h1>
            <p className="text-gray-600">@johndoe</p>
          </div>
        </div>

        <div className="mt-8">
          <h2 className="text-xl font-semibold text-gray-800 mb-4">
            Biography
          </h2>
          <p className="text-gray-700">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
        </div>

        <div className="mt-8">
          <h2 className="text-xl font-semibold text-gray-800 mb-4">
            Contact Information
          </h2>
          <ul className="text-gray-700">
            <li>Email: johndoe@example.com</li>
            <li>Phone: (123) 456-7890</li>
            {/* Ajoutez d'autres informations de contact au besoin */}
          </ul>
        </div>

        <div className="mt-8">
          <div className="bg-gray-200 p-4 rounded-md">
            <h2 className="text-xl font-semibold text-gray-800 mb-4">Skills</h2>
            <ul className="text-gray-700">
              <li>React.js</li>
              <li>Tailwind CSS</li>
              <li>JavaScript</li>
              {/* Ajoutez d'autres compétences au besoin */}
            </ul>
          </div>
        </div>

        <div className="mt-8">
          <button className="bg-indigo-500 text-white px-4 py-2 rounded-md hover:bg-indigo-600 focus:outline-none focus:ring focus:border-indigo-300">
            Edit Profile
          </button>
        </div>
      </div>
    </div>
  );
};

export default Account;

import React, { useState, useEffect, useContext } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Header from "../Header";
import "../../App.css";
import Footer from "../Footer";
import Landing from "../Landing";
import ErrorPage from "../ErrorPage";
import Login from "../Login";
import Signup from "../Signup";
import ForgetPassword from "../ForgetPassword";
import Account from "../Account";
/*import { FirebaseContext } from "../Firebase";
import firebase from "../Firebase/firebase";
import { auth } from "../Firebase/firebase";*/

// faire l'auth ici pour pouvoir faire le reload du header

function App() {
  //const firebase = useContext(FirebaseContext);

  /*  useEffect(() => {
    let listener = auth.onAuthStateChanged((user) => {
      user && setLogState(user);
    });

    if (logState) auth.signOut();

    return () => {
      listener();
    };
  }, [logState, auth]); */

  return (
    <Router>
      <Header />
      <div className="flex flex-col min-h-screen">
        <div className="flex-grow">
          <Routes>
            <Route exact path="/" element={<Landing />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/login" element={<Login />} />
            <Route path="*" element={<ErrorPage />} />
            <Route path="/account" element={<Account />} />
            <Route path="/forgetpassword" element={<ForgetPassword />} />
          </Routes>
        </div>
      </div>
      <Footer />
    </Router>
  );
}

export default App;
